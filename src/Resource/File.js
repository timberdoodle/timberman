/*
| A file to be REST-served.
*/
def.extend = 'Resource/Base/Data';

def.attributes =
{
	// absolute path of file the resource is to be loaded from
	aPath: { type: 'ti2c:Path' },

	// "binary" or "utf-8"
	coding: { type: [ 'undefined', 'string' ] },

	// mime type
	mime: { type: [ 'undefined', 'string' ] },

	// if true wraps the bundle in an async
	wrapAsync: { type: 'boolean', defaultValue: 'false' },
};

import fs from 'node:fs/promises';

import { Self as BaseData  } from '{Resource/Base/Data}';
import { Self as FileTypes } from '{FileTypes}';

/*
| Shortcut.
*/
def.static.APathAgeHeaderAsync =
	function( aPath, age, header, wrapAsync )
{
	const ext = aPath.ext;

	if( !ext ) throw new Error( );

	const mime = FileTypes.mime( ext );
	if( !mime ) throw new Error( );

	const coding = FileTypes.coding( ext );
	if( !coding ) throw new Error( );

	return(
		Self.create(
			'age', age,
			'aPath', aPath,
			'coding', coding,
			'header', header,
			'mime', mime,
			'wrapAsync', wrapAsync,
		)
	);
};

/*
| Shortcut.
*/
def.static.APath =
	function( aPath )
{
	return Self.APathAgeHeaderAsync( aPath, undefined, undefined, false );
};

/*
| Shortcut.
*/
def.static.APathAsync =
	function( aPath )
{
	return Self.APathAgeHeaderAsync( aPath, undefined, undefined, true );
};

/*
| Shortcut.
*/
def.static.APathLong =
	function( aPath )
{
	return Self.APathAgeHeaderAsync( aPath, 'long', undefined, false );
};

/*
| Shortcut.
*/
def.static.APathShort =
	function( aPath )
{
	return Self.APathAgeHeaderAsync( aPath, 'short', undefined, false );
};

/*
| Shortcut.
*/
def.static.APathSameOrigin =
	function( aPath )
{
	return Self.APathAgeHeaderAsync( aPath, undefined, BaseData.headerSameOrigin, false );
};

/*
| Shortcut.
*/
def.static.APathShortSameOrigin =
	function( aPath )
{
	return Self.APathAgeHeaderAsync( aPath, 'short', BaseData.headerSameOrigin, false );
};

/*
| Returns the code of the resource for the bundle.
|
| ~doAsync: if true pack it in a async
*/
def.proto.bundlize =
	function( result )
{
	// terser currently needs this kind of capsule to be efficient

	result.addString(
		'('
		+ ( this.wrapAsync ? 'async' : '' )
		+ '()=>{/*' + this.aPath.asString + '*/\n'
	);

	// FIXME provide a proper sourceMap
	result.addString( this.data );

	result.addString( '})();\n' );
};

/*
| Prepares the resource.
*/
def.proto.prepare =
	async function( )
{
	// if data already read, already prepared
	if( this.data ) return this;

	// note, do not convert data here to a string, otherwise binary fonts break.
	// FIXME do it for coding = utf-8
	const data = await fs.readFile( this.aPath.asString );

	return this.create( 'data', data );
};

