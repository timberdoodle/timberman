/*
| An ajax handler.
*/
def.extend = 'Resource/Base/Self';

def.attributes =
{
	// ajax handler function
	handler: { type: 'function' },
};

/*
| Does not have a file.
*/
def.proto.aPath = undefined;

/*
| Shortcut.
*/
def.static.Handler =
	function( handler )
{
	return Self.create( 'handler', handler );
};

/*
| Handles requests.
|
| ~request:  the http(s) request
| ~result:   the http(s) result
| ~ti2cWeb:  the ti2cWeb instance asking for handling
*/
def.proto._handle =
	async function( request, result, ti2cWeb )
{
	const data = [ ];

	if( request.method !== 'POST' )
	{
		ti2cWeb.webError( result, 400, 'Must use POST' );
		return;
	}

	request.on(
		'close',
		( ) =>
		{
			this.handler( 'close', undefined, result );
		}
	);

	request.on(
		'data',
		( chunk ) =>
		{
			data.push( chunk );
		}
	);

	const handler =
		async ( ) =>
	{
		const query = data.join( '' );
		let json;

		try
		{
			json = JSON.parse( query );
		}
		catch( err )
		{
			ti2cWeb.webError( result, 400, 'Not valid JSON' );
			return;
		}

		const asw = await this.handler( 'json', json, result );

		if( !asw ) return;

		result.writeHead(
			200,
			{
				'content-type': 'application/json',
				'cache-control': 'no-cache',
				'date': new Date().toUTCString()
			}
		);

		result.end( asw.jsonfy( ) );
	};

	request.on( 'end', handler );
};

