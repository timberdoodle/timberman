/*
| A twig of resources.
*/
def.extend = 'twig@<Resource/Types';

import { Self as ResourceTi2cClass } from '{Resource/Ti2c/Class}';
import { Self as ResourceTi2cCode  } from '{Resource/Ti2c/Code}';

/*
| Appends a ti2c walk twig.
*/
def.proto.appendTi2cWalk =
	function( wt )
{
	let tt = this;
	for( let key of wt.keys )
	{
		const res = wt.get( key );

		switch( res.ti2ctype )
		{
			case ResourceTi2cClass:
			{
				const resCode = res.tcResource;
				tt =
					tt.create(
						'twig:add', resCode.name, resCode,
						'twig:add', key, res,
					);
				break;
			}

			// collections only have generated code
			case ResourceTi2cCode:
			{
				tt = tt.create( 'twig:add', key, res );
				break;
			}
		}
	}
	return tt;
};

/*
| Appends another twig to this.
*/
def.proto.appendTwig =
	function( ot )
{
	let tt = this;
	for( let key of ot.keys )
	{
		tt = tt.create( 'twig:add', key, ot.get( key ) );
	}
	return tt;
};

/*
| Prepares all resources in the twig.
*/
def.proto.prepare =
	async function( )
{
	const keys = this.keys;
	const twig = { };

	for( let key of keys )
	{
		twig[ key ] = await this.get( key ).prepare( );
	}

	return this.create( 'twig:init', twig, keys );
};

/*
| Renames a resource.
*/
def.proto.rename =
	function( from, to )
{
/**/if( CHECK )
/**/{
/**/	if( arguments.length !== 2 ) throw new Error( );
/**/	if( typeof( from ) !== 'string' ) throw new Error( );
/**/	if( typeof( to ) !== 'string' ) throw new Error( );
/**/}

	const res = this.get( from );
	const rank = this.rankOf( from );

	return(
		this.create(
			'twig:remove', from,
			'twig:insert', to, rank, res,
		)
	);
};


