/*
| Generated ti2c code to be REST-served.
*/
def.extend = 'Resource/Base/Data';

def.attributes =
{
	// absolute path of ti2c generated code
	aPath: { type: 'ti2c:Path' },

	// type id of the ti2c resource
	type: { type: '<ti2c:Type/Ti2c/Types' },

	// the name (aka URI location) of the resource
	name: { type: 'string' },

	// the original source code (without pre/postamble)
	_source: { type: [ 'undefined', 'string' ] },
};

import fs from 'node:fs/promises';

import { Self as Ast       } from '{ti2c:Ast}';
import { Self as AstBlock  } from '{ti2c:Ast/Block}';
import { Self as AstImport } from '{ti2c:Ast/Import}';
import { Self as Formatter } from '{ti2c:Format}';
import { Self as Parser    } from '{ti2c:Parser}';

const readOptions = Object.freeze( { encoding: 'utf8' } );

/*
| Is javascript.
*/
def.proto.coding = 'utf-8';
def.proto.mime = 'text/javascript';

/*
| Returns the code of the resource for the bundle.
|
| ~result: mutable protean to build sourcemapped code.
*/
def.proto.bundlize =
	function( result )
{
	const source = this._source;
	const pAst = Parser.block( source );
	const aList = [ ];

	for( let s of pAst.statements )
	{
		if( s.ti2ctype !== AstImport )
		{
			aList.push( s );
		}
		else
		{
			let mn = s.moduleName;
			if( !mn.startsWith( '{{' ) ) throw new Error( );
			if( !mn.endsWith( '}}' ) ) throw new Error( );
			mn = mn.substr( 2, mn.length - 4 );

			let expr = Ast.var( '__ti2cEntries' );
			expr = Ast.member( expr, Ast.string( mn ) );
			expr = Ast.dot( expr, 'Self' );

			aList.push( Ast.const( s.aliasName, expr ) );
		}
	}

	const tAst = AstBlock.Array( aList );

	// terser currently needs this kind of capsule to be efficient
	result.addString( '(()=>{/*' + this.name + '*/\n' );
	result.addString( this._getPreamble( true ) );
	Formatter.formatWithSourceMap( tAst, this.name, result );
	result.addString( '})();\n' );
};

/*
| Prepares this resource, adds pre and postamble.
*/
def.proto.prepare =
	async function( )
{
	if( this.data ) return this;

	const aPath = this.aPath;
	let source;
	try
	{
		source = await fs.readFile( aPath.asString, readOptions );
	}
	catch( e )
	{
		console.log( 'Cannot read "' + aPath.asString + '"' );
		throw new Error( );
	}
	source += '';

	const preamble = this._getPreamble( false );
	const postamble = '\nexport { Self };\n';

	return(
		this.create(
			'data', preamble + source + postamble,
			'_source', source,
		)
	);
};

/*
| Returns the preamble to be prepended
| to sources for browser mode.
|
| ~bundlize: true if bundlizing / false for devel mode
*/
def.proto._getPreamble =
	function( bundlize )
{
	return 'const {Self}=__ti2cEntries[\'' + this.type.asString + '\'];';
};

