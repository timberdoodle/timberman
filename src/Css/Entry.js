/*
| A css entry.
*/
def.attributes =
{
	// the css selector
	selector: { type: 'string' },

	// the css attributes
	attributes: { type: 'group@string' },
};

import { Self as GroupString } from '{group@string}';

/*
| Shortcut.
*/
def.static.SP =
	function( selector, protean )
{
	const group = { };
	const keys = Object.keys( protean );

	for( let key of keys )
	{
		const dkey = key.replaceAll( '_', '-' );
		group[ dkey ] = protean[ key ];
	}

	return(
		Self.create(
			'selector', selector,
			'attributes', GroupString.Table( group ),
		)
	);
};

/*
| Returns the css entry as text.
*/
def.lazy.text =
	function( )
{
	let t = this.selector + '\n{';
	const attr = this.attributes;
	for( let key of attr.keys )
	{
		t += '\n\t' + key + ' : ' + attr.get( key ) + ';';
	}
	t += '\n}\n';
	return t;
};
