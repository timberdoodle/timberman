/*
| A css sheet
*/
def.extend = 'group@Css/Entry';

import { Self as CssEntry } from '{Css/Entry}';

/*
| Shortcut.
*/
def.static.SPGroup =
	function( group )
{
	const g = { };
	const keys = Object.keys( group );


	for( let key of keys )
	{
		g[ key ] = CssEntry.SP( key, group[ key ] );
	}

	return Self.Table( g );
};

/*
| Returns the css sheet as text.
*/
def.lazy.text =
	function( )
{
	let t = '';
	for( let key of this.keys )
	{
		if( t !== '' ) t += '\n';
		t += this.get( key ).text;
	}
	return t;
};
