/*
| A mutable(!) protean to build sourcemapped code.
*/
import { GenMapping, maybeAddMapping, toDecodedMap, toEncodedMap } from '@jridgewell/gen-mapping';

export class Result
{
	#lineNr = 1;
	#column = 0;
	#map;

	/*
	| Constructor.
	*/
	constructor( outputName )
	{
		this.code = '';
		this.#map = new GenMapping( { file: outputName } );
	}

	/*
	| Adds a node
	*/
	addNode( node, sourceFilename )
	{
		const token = node.token;
		maybeAddMapping(
			this.#map,
			{
				generated: { line: this.#lineNr, column: this.#column },
				name: node.string,
				original: { line: token.lineNr, column: token.column },
				source: sourceFilename,
			}
		);
		this.addString( node.string );
	}

	/*
	| Adds a string.
	*/
	addString( s )
	{
		let ion = 0;
		this.code += s;
		for(;;)
		{
			const nion = s.indexOf( '\n', ion );
			if( nion < 0 )
			{
				this.#column += s.length - ion;
				break;
			}
			ion = nion + 1;
			this.#column = 0;
			this.#lineNr++;
		}
	}

	/*
	| Returns the decoded map.
	*/
	decodedMap( )
	{
		return toDecodedMap( this.#map );
	}

	/*
	| Returns the encoded map.
	*/
	encodedMap( )
	{
		return toEncodedMap( this.#map );
	}
}
