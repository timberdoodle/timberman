/*
| The ti2c-web middleware.
*/
def.attributes =
{
	// if defined calls this function for logging
	log: { type: [ 'undefined', 'function' ] },

	// if false requests the client to not cache anything
	requestCaching: { type: 'boolean' },

	// the resources
	resources: { type: [ 'undefined', 'Resource/Twig' ] },
};

import { Self as ResourceFile   } from '{Resource/File}';
import { Self as ResourceMemory } from '{Resource/Memory}';
import { Self as ResourceTwig   } from '{Resource/Twig}';

/*
| A twig of the ti2c base resources.
*/
def.staticLazy.ti2cBaseResources =
	function( )
{
	const aDirTi2cCore = ti2c.getPackage( 'ti2c' ).srcDir.d( 'core' );

	return(
		ResourceTwig.create(
			'twig:add', 'ti2c/packages-init.js',
			ResourceMemory.JsData( '' ),

			'twig:add', 'ti2c-web/browser.js',
			ResourceFile.APath( ti2c.getPackage( 'web' ).srcDir.f( 'browser.js' ) ),

			'twig:add', 'ti2c/core/common.js',
			ResourceFile.APath( aDirTi2cCore.f( 'common.js' ) ),

			'twig:add', 'ti2c/core/timur32.js',
			ResourceFile.APath( aDirTi2cCore.f( 'timur32.js' ) ),
		)
	);
};

/*
| Returns the pathname for a request.
*/
def.proto.pathname =
	function ( request )
{
	try
	{
		const url = new URL(`http://${request.headers.host}${request.url}`);
		return decodeURI( url.pathname ).replace( /^[/]+/g, '' );
	}
	catch( e )
	{
		if( this.log ) this.log( e.message );
		return undefined;
	}
};

/*
| Listens to http(s) requests.
|
| ~request:  http(s) request
| ~result:   http(s) result
| ~pathname: parsed pathname (if undefined parses itself)
*/
def.proto.requestListener =
	async function( request, result, pathname )
{
	if( this.log )
	{
		this.log( request.socket.remoteAddress, request.url );
	}

	if( pathname === undefined )
	{
		pathname = this.pathname( request );
	}

	const res = this.resources.get( pathname );
	if( !res )
	{
		this.webError( result, 404, 'Bad Request' );
		return;
	}

	res._handle( request, result, this );
};

/*
| Logs and returns a web error.
*/
def.proto.webError =
	function( result, code, message )
{
	result.writeHead(
		code,
		{
			'content-type': 'text/plain',
			'cache-control': 'no-cache',
			'date': new Date( ).toUTCString( )
		}
	);

	message = code + ' ' + message;

	if( this.log )
	{
		this.log( 'web error', code, message );
	}

	result.end( message );
};
